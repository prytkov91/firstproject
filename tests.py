"""Testing"""
import unittest
import app as myapi
class TestFlaskApi(unittest.TestCase):
    """setup"""
    def setUp(self):
        self.app = myapi.app.test_client()
    """get testing"""
    def test_get(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)
    """post testing"""
    def test_post(self):
        response = self.app.post('/')
        self.assertEqual(response.status_code, 405)
if __name__ == '__main__':
    unittest.main()
