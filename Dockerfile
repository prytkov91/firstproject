FROM python:3.8-slim-buster
COPY . /app
WORKDIR /app
RUN ls
RUN python -m pip install -r requirements.txt
CMD [ "python3", "-m" , "flask", "run"]